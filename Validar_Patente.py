# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Simple función para validar patentes Mercosur
# El sistema puede diferenciar entre patentes viejas (Argentina) y nuevas Mercosur
# En el caso de patentes malformadas devolverá el valor "errónea"
# -------------------------------------------------------------------------


def tipo_patente(patente):
    patente=patente.lower().replace(' ', '')
    puntos=0
    datos_patente=[0,0]
    letras = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    numeros =['0','1','2','3','4','5','6','7','8','9']    

#Comprobar patente vieja Formato ABC123
    if len(patente)==6:
        for elemento in patente[0:3]:
            if elemento in letras: puntos+=1

        for elemento in patente[3:6]:
            if elemento in numeros: puntos+=1
        if puntos==6: return "Vieja argentina"
#Comprobar patente Mercosur (Arg, Brasil, Venezuela) Formato: AB123AC
    if len(patente)==7:
        puntos=0
        for elemento in patente[0:2]:
            if elemento in letras: puntos+=1

        for elemento in patente[2:5]:
            if elemento in numeros: puntos+=1
        for elemento in patente[5:7]:
            if elemento in letras: puntos+=1
        if puntos==7: return "Mercosur (Arg, Brasil, Ven)"
    
    #Comprobar patente Mercosur (Paraguay) Formato: 123ABCD
    if len(patente)==7:
        puntos=0
        for elemento in patente[0:3]:
            if elemento in numeros: puntos+=1
        for elemento in patente[3:7]:
            if elemento in letras: puntos+=1
        if puntos==7: return "Mercosur Paraguay"
    
    #Comprobar patente Mercosur (Uruguay) Formato: ABC1234
    if len(patente)==7:
        puntos=0
        for elemento in patente[0:3]:
            if elemento in letras: puntos+=1
        for elemento in patente[3:7]:
            if elemento in numeros: puntos+=1
        if puntos==7: return "Mercosur Uruguay"
    
#Comprobacion del tipo de patente
    datos_patente=[len(patente),puntos]
    if datos_patente!=[6,6] and datos_patente!=[7,7]: return "Erronea"
